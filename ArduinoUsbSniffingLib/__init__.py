import serial
import time
from os import path


class ArduinoUsbSniffing:
    def __init__(self):
        logo = self.art()
        for line in logo:
            print(line)
        print('')

    @staticmethod  # so I dont have to include self
    def recieve_usb(port, datapoints=5, output_file='raw_usb_data.txt', baud_rate=2000000, verbosity=0):
        """recieves serial communications from arduino and writes results to a text file
        10000 ~ 5.7466325759887695 seconds

        Arguments:
            datapoints:  the number of data to sniff with the arduino
            output_file:  the file the raw data will be written to
            port:  the port the arduino is connected to; the same as listed in the Tools tab of ArduinoIDE
            baud_rate:  the Serial communication rate of the Arduino
            verbosity:  determines how will printed live out as data is collected
                0 = no information
                1 += time to execute
                2 += every message"""
        now = time.time()

        try:
            ard = serial.Serial(port, baud_rate, timeout=5)
            print('Arduino Connected :D\n')
        except:
            raise Exception('No Arduino detected.\nCheck that the provided port:  '+port+'  is correct.')

        f = open(output_file, 'w+')
        i = 0
        while i < datapoints:
            msg = ard.readline()
            if verbosity > 1:
                print("Message from arduino:", msg)
            f.write(str(msg) + '\n')
            i += 1

        future = time.time()
        if verbosity == 2:
            print('')
        if verbosity > 0:
            print('Time to collect data:', future - now)

    @staticmethod
    def file_len(file):
        """returns the number of lines in a file
        Arguments:
            file:  the filepath of a file"""
        with open(file) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

    @staticmethod
    def clean_usb_data(raw_file='raw_usb_data.txt'):
        """takes raw data and returns cleaned data w/out arduino gibberish
        Arguments:
            raw_file:  the filepath to raw data to be cleaned

        Returns (3 variables)
            data from the "data plus" pin of USB
            data from the "data minus" pin of USB
            the total number of errors made in data collection that cannot be cleaned
                oftentimes the first message from the Arduino announces it's connection and is counted as an error"""

        data_plus = []
        data_minus = []
        errors = 0
        with open(raw_file, 'r') as f:
            lines = f.readlines()  # line is read as a string not bytes
            for line in lines:
                linelen = len(line)
                if linelen == 24:
                    data_plus.append(line[8])
                    data_minus.append(line[17])
                else:
                    errors += 1
                    print('line error:  incorrect length of', str(linelen) + '; length should be 24')
            print('total errors:', errors)
            return data_plus, data_minus, errors

    @staticmethod
    def art():
        """returns a list of strings, each of which is a line of the Arduino_USB_Sniffer logo."""
        ascii_path = path.split(__file__)[0] + '/Arduino_USB_Sniffer_ASCII_art.txt'
        ascii_art = []
        print(ascii_path)
        with open(ascii_path, 'r') as f:
            raw_art = f.readlines()
        for line in raw_art:
            ascii_art.append(line.replace('\n', ''))
        return ascii_art

    def clean_to_file(self, raw_file='raw_usb_data.txt', output_file='clean_usb_data.csv'):
        """cleans given raw data and puts it in a file
        Arduments:
            raw_file:  the filepath to raw data to be cleaned
            output_file:  the filename to create or overwrite the file of the clean data"""
        length = self.file_len(raw_file)
        with open(output_file, 'w+') as f:
            data_plus, data_minus, errors = self.clean_usb_data()
            for i in range(length - errors):
                f.write(data_plus[i] + ',' + data_minus[i] + '\n')
