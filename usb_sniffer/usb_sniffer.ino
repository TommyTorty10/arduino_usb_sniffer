const int data_plus_pin = 7;
const int data_minus_pin = 8;
int data_plus = 0;
int data_minus = 0;
String outstring;

void setup() {
  pinMode(data_plus_pin, INPUT);
  Serial.begin(500000);  // arduino pro mini no more than 500K; arduino uno 2M
  pinMode(data_minus_pin, INPUT);

}

void loop() {
  data_plus = digitalRead(data_plus_pin);
  data_minus = digitalRead(data_minus_pin);

  outstring = "dplus "+String(data_plus)+" dminus "+String(data_minus);
  Serial.println(outstring);

}
